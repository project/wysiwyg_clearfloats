wysiwyg_clearfloats is a WYSIWYG plugin that enables 3 additional buttons on the TinyMCE editor.  These buttons allow you to insert a breaking element that will clear floated elements above it.  This is very handy when inserting pictures that you wish to be floated left, but then require your next paragraph to start 'below' the floated elements.  Clearing the float enables you to do this.

Drupal module written Clifford Meece (meecect)
http://cliffordmeece.com

The bulk of the work is done by the tinyMCE plugin component, which was written by Miguel Ibero as a plugin for wordpress (http://wordpress.org/extend/plugins/tinymce-clear-buttons)

Here is information on the original wordpress plugin: 

/*
Plugin Name: TinyMCE Clear
Plugin URI: http://www.peix.org/code/tinymce-clear-buttons
Description: Enables TinyMCE clear buttons
Version: 1.1
Author: Miguel Ibero
Author URI: http://www.peix.org

Released under the GPL v.2, http://www.gnu.org/copyleft/gpl.html

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
*/



